module git.sindominio.net/estibadores/wkd

go 1.17

require (
	github.com/emersion/go-openpgp-wkd v0.0.0-20200304100729-bbe7fac61be2
	github.com/go-ldap/ldap/v3 v3.1.8
	github.com/namsral/flag v1.7.4-pre
	github.com/go-asn1-ber/asn1-ber v1.3.1 // indirect
	github.com/tv42/zbase32 v0.0.0-20190604154422-aacc64a8f915 // indirect
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2 // indirect
)

replace github.com/emersion/go-openpgp-wkd => /home/user/wkd/go-openpgp-wkd
