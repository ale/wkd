package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"github.com/emersion/go-openpgp-wkd"
	"github.com/go-ldap/ldap/v3"
	"github.com/namsral/flag"
)

const (
	openPGPhashAtrribute = "openPGPhash"
	openPGPkeyAtrribute  = "openPGPkey"
	mailAtrribute        = "mail"
)

func main() {
	var (
		bind   = flag.String("bind", ":8080", "bind address for the web server")
		addr   = flag.String("addr", "localhost:389", "LDAP server address and port")
		user   = flag.String("user", "", "Username to bind to the LDAP")
		pass   = flag.String("pass", "", "Password of the LDAP user")
		baseDN = flag.String("baseDN", "", "LDAP baseDN for searches")
		filter = flag.String("filter", "(objectClass=posixAccount)", "LDAP filter for searches")
	)
	flag.String(flag.DefaultConfigFlagname, "/etc/wkd.conf", "Path to configuration file")
	flag.Parse()

	d := discoverer{
		addr:   *addr,
		user:   *user,
		pass:   *pass,
		baseDN: *baseDN,
		filter: *filter,
	}
	handler := wkd.Handler{
		Discover: d.Discover,
	}
	http.ListenAndServe(*bind, &handler)
}

type discoverer struct {
	addr   string
	user   string
	pass   string
	baseDN string
	filter string
}

func (d *discoverer) Discover(hash, domain, localPart string) (io.Reader, error) {
	entry, err := d.getLDAPentry(hash)
	if err != nil {
		return nil, err
	}

	mail := entry.GetAttributeValue(mailAtrribute)
	mailParts := strings.Split(mail, "@")
	if len(mailParts) != 2 {
		log.Println("Wrong mail format on the user record:", mail)
		return nil, wkd.ErrNotFound
	}
	if (localPart != "" && mailParts[0] != localPart) || mailParts[1] != domain {
		log.Printf("The requested email %s@%s doesn't match with the user found %s", localPart, domain, mail)
		return nil, wkd.ErrNotFound
	}

	key := entry.GetRawAttributeValue(openPGPkeyAtrribute)
	if len(key) == 0 {
		return nil, wkd.ErrNotFound
	}

	return bytes.NewReader(key), nil
}

func (d *discoverer) connect() (*ldap.Conn, error) {
	conn, err := ldap.Dial("tcp", d.addr)
	if err != nil {
		return nil, err
	}
	err = conn.Bind(d.user, d.pass)
	return conn, err
}

func (d *discoverer) getLDAPentry(hash string) (*ldap.Entry, error) {
	searchAttributes := []string{mailAtrribute, openPGPkeyAtrribute}

	conn, err := d.connect()
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	searchRequest := ldap.NewSearchRequest(d.baseDN,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf("(&%s(%s=%s))", d.filter, openPGPhashAtrribute, ldap.EscapeFilter(hash)),
		searchAttributes,
		nil,
	)

	sr, err := conn.Search(searchRequest)
	switch len(sr.Entries) {
	case 1:
		entry := sr.Entries[0]
		return entry, nil
	case 0:
		return nil, wkd.ErrNotFound
	default:
		return nil, fmt.Errorf("Unexpected number of matches for the hash: %s %d", hash, len(sr.Entries))
	}
}
